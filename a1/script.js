let output = document.getElementById("result")
let operationButton = document.getElementsByClassName("operation")

for(x=0;x<operationButton.length;x++){
	operationButton[x].addEventListener('click', (e)=>{
		pass = e.target.innerHTML
		calculate(pass)
	})
}

const calculate = (sign)=>{
	let input1 = document.querySelector("#input1").value
	let input2 = document.querySelector("#input2").value
	if(sign == 'Addition'){
		result = Number(input1) + Number(input2)
		output.innerHTML = result
	}else if(sign == 'Subtraction'){
		result = Number(input1) - Number(input2)
		output.innerHTML = result
	}else if(sign == 'Multiplication'){
		result = Number(input1) * Number(input2)
		output.innerHTML = result
	}else if(sign == 'Modulo'){
		result = Number(input1) % Number(input2)
		output.innerHTML = result
	}else if(sign == 'Division'){
		result = Number(input1) / Number(input2)
		output.innerHTML = result
	}
}
//Gets the user button with an id of btn1 using querySelector
//If query selector, # means we are selecting and ID, . means we are selecting a class
let btn1 = document.querySelector("#btn1")

//Add an EventListener that calls a prompt box

btn1.addEventListener("click", ()=>{
	let userInput = prompt("Enter Name");
	document.getElementById("output").innerHTML = userInput;
})